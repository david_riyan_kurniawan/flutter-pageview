import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  //membuat initial page controller
  final PageController _pageController = PageController(initialPage: 0);

  //membuat current index yang bernama _activePage
  int _activePage = 0;

  final List _pages = [
    FirstPage(),
    SecondPage(),
    ThirdPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('PAGE VIEW'),
        ),
        body: Stack(
          children: [
            PageView.builder(
              scrollDirection: Axis.vertical,
              controller: _pageController,
              onPageChanged: (int page) {
                setState(() {
                  _activePage = page;
                });
              },
              itemBuilder: (BuildContext, int index) {
                return _pages[index % _pages.length];
              },
              itemCount: _pages.length,
            ),
            //membuat indicator page view
            Positioned(
              width: 50,
              bottom: 0,
              right: 0,
              top: 0,
              child: Container(
                color: Colors.black45,
                //membuat column untuk menampung indicator
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List<Widget>.generate(
                      _pages.length,
                      (index) => Padding(
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            child: InkWell(
                              onTap: () {
                                _pageController.animateToPage(index,
                                    duration: Duration(milliseconds: 300),
                                    curve: Curves.easeIn);
                              },
                              //membuat bentuk indicator, disini saya menggunakan circle avatar
                              //agar bentuk indicatornya menjadi lingkaran
                              child: CircleAvatar(
                                radius: 10,
                                backgroundColor: _activePage == index
                                    ? Colors.amber
                                    : Colors.black,
                              ),
                            ),
                          )),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

//membuat class first page
class FirstPage extends StatelessWidget {
  const FirstPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.red,
      ),
    );
  }
}

//membuat class second page
class SecondPage extends StatelessWidget {
  const SecondPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.blue,
      ),
    );
  }
}

//membuat class third page
class ThirdPage extends StatelessWidget {
  const ThirdPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.green,
      ),
    );
  }
}
